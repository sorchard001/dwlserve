## dwlserve.py

Minimal DriveWire server written in python.

Implements a small subset of functions mainly to support Tormod Volden's dwload software for Dragon computers.

Can be configured to serve via hardware serial or Becker port.

### How to use

Navigate to a folder containing files and disk images to be served, then run **dwlserve.py**. The server is small enough to place directly in the folder, or it may be placed on the executable path for convenience.

Options may be specified either on the command line or a configuration file named **dwlserve.cfg**.

#### Command line options

| Option      | Description                       |
|:------------|:----------------------------------|
|-b, --becker | Serve via Becker port             |
|-s, --serial | Serve via serial port             |
|-l, --log    | Enable command logging to console |

The serial and network settings are either specified in the dwlserve.cfg file or fall back to defaults hard-coded in dwlserve.py (These defaults can be found near the top of dwlserve.py and may be edited for convenience)

If no server option is specified then one will be chosen based on what has been specified in the configuration file. If none are found or both have been specified, then the server will default to serial.

### dwlserve.cfg example

```
[serport]
port = /dev/ttyUSB0
baud = 57600

[becker]
addr = 127.0.0.1
port = 65504

[drive0]
name = nos9.dsk
readonly = 1

[drive1]
name = scratch.dsk
readonly = 0
```

Specifying a serial port for Windows:

```
[serport]
port = COM1
baud = 57600
```

If no disk images are specified then all disk images in the folder will be mounted as read-only in the order that they are found. The only disk image type supported is the .dsk format which is a simple headerless file of sectors.

### Prerequisites

- [python3](https://www.python.org/)
- [pyserial](https://pypi.org/project/pyserial/)
- [dwload software](http://archive.worldofdragon.org/phpBB3/viewtopic.php?f=5&t=4964)

### Tip o' the hat to...

- [Tormod Volden](http://tormod.me/drivewire.html) - Dragon DriveWire hardware and software
- [The DriveWire 4 Team](https://sourceforge.net/p/drivewireserver/_members/)
- [Ciaran Anscomb](https://www.6809.org.uk/) - xroar emulator and cross development tools
