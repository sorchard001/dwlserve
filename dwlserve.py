#!/usr/bin/python3

# dwlserve.py
# Minimal Dragon DriveWire server
# S.Orchard 2019-2021
#
# Implements a subset of op codes mainly to
# support Tormod Volden's DWLOAD software
#
# Serves either serial port or Becker port

import argparse
import serial
import socket
import sys
import time
import configparser
import glob
from datetime import datetime
from functools import reduce

# default serial setup
DW_DEFAULT_SER_PORT = '/dev/ttyUSB0'
#DW_DEFAULT_SER_PORT = 'COM1'
DW_DEFAULT_SER_BAUD = 57600

# default Becker setup
# listen for connections from this machine only
#DW_DEFAULT_BECKER_ADDR = '127.0.0.1'
# listen on any address that this machine has
DW_DEFAULT_BECKER_ADDR = ''
DW_DEFAULT_BECKER_PORTNO = 65504

# maximum allowed lsn when extending file
DW_MAX_EXT_LSN = 65535

# minimum drive number allowed for named objects
DW_MIN_NAMED_OBJ = 0xf0

# expiry time of named objects (seconds)
DW_OBJ_EXPIRY_S = 5

# server error codes sent to client
DW_ERR_WP = 242
DW_ERR_CHECKSUM = 243
DW_ERR_READ = 244
DW_ERR_WRITE = 245
DW_ERR_NOTREADY = 246


class DWError(Exception):
    def __init__(self, message=''):
        self.message = message
    def __str__(self):
        return self.message

class DWTimeoutError(DWError):
    def __init__(self):
        self.message = 'Comms timeout'

class DWWriteProtectError(DWError):
    def __init__(self):
        self.message = 'Write protected file'

class DWEmptyReadError(DWError):
    def __init__(self):
        self.message = 'Read beyond end of file'

class DWTooLargeError(DWError):
    def __init__(self):
        self.message = 'File too large'

class DWNoFreeObjectsError(DWError):
    def __init__(self):
        self.message = 'No free objects'

class DWDisconnected(DWError):
    def __init__(self):
        self.message = 'Client disconnected'


# class that represents a disk file or named object
class DWObject():

    def __init__(self, name):
        self.name = name
        self.expiryseconds = 0
        self.wp = False
        self.lastaccess = datetime.now()

    def readlsn(self, lsn):
        self.lastaccess = datetime.now()
        with open(self.name, 'rb') as f:
            f.seek(lsn*0x100)
            dat = f.read(0x100)
            if len(dat) == 0:
                raise DWEmptyReadError
        dat += bytes(0x100 - len(dat))
        return dat

    def writelsn(self, lsn, sector):
        self.lastaccess = datetime.now()
        if self.wp:
            raise DWWriteProtectError
        writepos = lsn*0x100
        with open(self.name, 'rb+') as f:
            f.seek(0, 2)
            while f.tell() < writepos:
                if lsn > DW_MAX_EXT_LSN:
                    raise DWTooLargeError
                f.write(bytes(0x100))
            f.seek(writepos)
            f.write(sector)


# serial connection class
class DWSerialConn(serial.Serial):

    def __init__(self, *args, **kwargs):
        kwargs['timeout'] = 1
        super().__init__(*args, **kwargs)


# Becker port connection class
class DWBeckerConn(socket.socket):
    def __init__(self, hostaddr, portno, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.beckeraddr = hostaddr
        self.beckerportno = portno
        self.clientconn = None

    def getconn(self):
        if self.clientconn is None:
            self.bind((self.beckeraddr, self.beckerportno))
            self.settimeout(1)  # set timeout to avoid blocking
            self.listen()
            print ('Listening on port {}'.format(self.beckerportno))
            while self.clientconn is None:
                try:
                    self.clientconn, addr = self.accept()
                    self.close()    # stop listening for clients
                except socket.timeout:
                    pass

            print('Connected to {}'.format(addr))
            self.clientconn.settimeout(1)     # set timeout to avoid blocking

    def read(self, nbytes):
        if self.clientconn is None:
            self.getconn()
        try:
            rxbuf = bytearray(b'')
            while len(rxbuf) < nbytes:
                tstart = time.time()
                rxdata = self.clientconn.recv(nbytes - len(rxbuf))
                # if recv returns quickly with no data then this
                # means the client has disconnected
                if (len(rxdata) == 0) and (time.time() - tstart < 0.99):
                    self.clientconn.close()
                    # get new socket ready
                    self.__init__(self.beckeraddr, self.beckerportno)
                    raise DWDisconnected

                rxbuf.extend(rxdata)

        except ConnectionResetError:
            self.clientconn.close()
            # get new socket ready
            self.__init__(self.beckeraddr, self.beckerportno)
            raise DWDisconnected

        except socket.timeout:
            rxbuf = bytearray(b'')

        if len(rxbuf) > nbytes:
            raise Exception('Excess data received')

        return bytes(rxbuf)

    def write(self, bytes):
        if self.clientconn is None:
            self.getconn()
        self.clientconn.send(bytes)


# DriveWire server class
class DWServer():

    def __init__(self, log=False):
        self.objects = {}
        self.conn = None
        self.log = log

    def getbyte(self):
        b = self.conn.read(1)
        if len(b) > 0:
            return b[0]
        else:
            raise DWTimeoutError()

    def putbyte(self, b):
        self.conn.write(bytes([b]))

    def getint(self, nbytes):
        n = self.conn.read(nbytes)
        if len(n) == nbytes:
            return reduce(lambda x,y: x*0x100 + y, n)
        else:
            raise DWTimeoutError()

    def getname(self):
        nlen = self.getbyte()
        return self.conn.read(nlen)

    def getdrivelsn(self):
        drive = self.getbyte()
        lsn = self.getint(3)
        return drive, lsn

    def checksum16(self, b):
        return sum(b) & 0xffff

    def reporterr(self, msg):
        tstamp = datetime.now().strftime('%d-%m-%y %H:%M:%S')
        print('[%s] %s' % (tstamp, msg))

    def reporterr_drv(self, msg, drive, lsn):
        self.reporterr('%s [%02x:%06x]' % (msg, drive, lsn))

    def op_nop(self):
        pass

    def allocate_named_object(self, name):
        drive = 0xff
        while True:
            if not drive in self.objects:
                obj = DWObject(name)
                break
            obj = self.objects[drive]
            if obj.expiryseconds > 0:
                tdelta = datetime.now() - obj.lastaccess
                if tdelta.total_seconds() > obj.expiryseconds:
                    break
            drive -= 1
            if drive < DW_MIN_NAMED_OBJ:
                raise DWNoFreeObjectsError

        obj.name = name
        obj.expiryseconds = DW_OBJ_EXPIRY_S
        self.objects[drive] = obj
        return drive


    def op_nameobj_mount(self):
        name = self.getname()
        if self.log:
            print('op_nameobj_mount  {}'.format(name))
        try:
            with open(name, 'rb') as f:
                pass
            drive = self.allocate_named_object(name)
        except Exception as E:
            self.reporterr('nameobj_mount failed [%s]\n\t(%s)' %
                    (name.decode('utf-8'), E))
            drive = 0
        self.putbyte(drive)
        if self.log:
            print('op_nameobj_mount  return status {}'.format(drive))


    def op_nameobj_create(self):
        name = self.getname()
        if self.log:
            print('op_nameobj_create  {}'.format(name))
        try:
            with open(name, 'xb') as f:
                pass
            drive = self.allocate_named_object(name)
        except Exception as E:
            self.reporterr('nameobj_create failed [%s]\n\t(%s)' %
                    (name.decode('utf-8'), E))
            drive = 0
        self.putbyte(drive)
        if self.log:
            print('op_nameobj_create  return status {}'.format(drive))


    def op_time(self):
        dt = datetime.now()
        year = (dt.year - 1900) & 0xff
        self.putbyte(year)
        self.putbyte(dt.month)
        self.putbyte(dt.day)
        self.putbyte(dt.hour)
        self.putbyte(dt.minute)
        self.putbyte(dt.second)
        if self.log:
            print('op_time')


    def op_write(self):
        drive, lsn = self.getdrivelsn()
        if self.log:
            print('op_write   drv {} lsn {}'.format(drive, lsn))
        sector = self.conn.read(0x100)
        chk_remote = self.getint(2)
        chk_local = self.checksum16(sector)
        if chk_local == chk_remote:
            if drive in self.objects:
                try:
                    self.objects[drive].writelsn(lsn, sector)
                    errcode = 0
                except DWWriteProtectError:
                    errcode = DW_ERR_WP
                    self.reporterr_drv('write protected', drive, lsn)
                except:
                    errcode = DW_ERR_WRITE
                    self.reporterr_drv('write fail', drive, lsn)
            else:
                errcode = DW_ERR_NOTREADY
                self.reporterr_drv('write not ready', drive, lsn)
        else:
            errcode = DW_ERR_CHECKSUM
            self.reporterr_drv(
                    'write checksum fail: local %02x remote %02x'
                    % (chk_local, chk_remote),
                    drive, lsn)

        self.putbyte(errcode)


    def op_readex(self):
        drive, lsn = self.getdrivelsn()
        if self.log:
            print('op_readex  drv {} lsn {}'.format(drive, lsn))
        if drive in self.objects:
            try:
                sector = self.objects[drive].readlsn(lsn)
                errcode = 0
            except Exception as E:
                errcode = DW_ERR_READ
                self.reporterr_drv('readex failed', drive, lsn)
                print(E)
        else:
            errcode = DW_ERR_NOTREADY
            self.reporterr_drv('readex not ready', drive, lsn)

        if errcode != 0:
            sector = bytes(0x100)

        chk_local = self.checksum16(sector)
        self.conn.write(sector)
        chk_remote = self.getint(2)

        if errcode == 0:
            if chk_local != chk_remote:
                errcode = DW_ERR_CHECKSUM
                self.reporterr_drv(
                    'readex checksum fail: local %02x remote %02x'
                    % (chk_local, chk_remote),
                    drive, lsn)

        self.putbyte(errcode)


    def op_unknown(self, op):
        self.reporterr('Unsupported opcode [0x%02x]' % op)


    def getcmd(self):
        op = {
            0x00: self.op_nop,
            0x01: self.op_nameobj_mount,
            0x02: self.op_nameobj_create,
            0x23: self.op_time,
            0x57: self.op_write,
            0xd2: self.op_readex
        }
        b = self.conn.read(1)
        if len(b) > 0:
            b = b[0]
            op.get(b, lambda: self.op_unknown(b))()


    def serverloop(self):
        while True:
            try:
                self.getcmd()
            except DWDisconnected as E:
                print(E)
            except serial.serialutil.SerialException as E:
                print('Exception: ', E)
                break;
            except (KeyboardInterrupt, SystemExit):
                print('\nExit')
                break
            except Exception as E:
                print('Exception: ', E)


if __name__ == '__main__':

    print('Minimal dwload server by S.Orchard 2019-2021')

    serport = None
    serbaud = None

    cfg = configparser.ConfigParser()
    cfg.read('dwlserve.cfg')

    serport = DW_DEFAULT_SER_PORT
    serbaud = DW_DEFAULT_SER_BAUD
    beckeraddr = DW_DEFAULT_BECKER_ADDR
    beckerport = DW_DEFAULT_BECKER_PORTNO

    choose_serial = True

    if cfg.has_section('becker'):
        beckeraddr = cfg['becker'].get('addr', DW_DEFAULT_BECKER_ADDR)
        beckerport = cfg['becker'].get('portno', DW_DEFAULT_BECKER_PORTNO)
        choose_serial = False

    if cfg.has_section('serport'):
        serport = cfg['serport'].get('port', DW_DEFAULT_SER_PORT)
        serbaud = cfg['serport'].getint('baud', DW_DEFAULT_SER_BAUD)
        choose_serial = True

    parser = argparse.ArgumentParser()
    parser.add_argument('-b', '--becker', action='store_true',
        help='Force Becker server')
    parser.add_argument('-s', '--serial', action='store_true',
        help='Force serial server')
    parser.add_argument('-l', '--log', action='store_true',
        help='Log commands to console')

    args = parser.parse_args()

    if args.serial:
        choose_serial = True
    elif args.becker:
        choose_serial = False

    try:
        dw = DWServer(args.log)
        if choose_serial:
            print('Serial server {} @ {} baud'.format(serport, serbaud))
            conn = DWSerialConn(serport, serbaud)
        else:
            print('Becker server host addr {}'.format(beckeraddr))
            conn = DWBeckerConn(beckeraddr, beckerport)

        dw.conn = conn

        # get disk defs from config file
        for section in cfg:
            if section[:5] == 'drive':
                dnum = int(section[5:])
                fname = cfg[section]['name']
                readonly = cfg[section].getboolean('readonly', True)
                obj = DWObject(fname)
                obj.wp = readonly
                dw.objects[dnum] = obj

        # nothing in config file so just mount any disk files
        # found in current folder as read only
        if len(dw.objects) == 0:
            for dnum, fname in enumerate(glob.glob('*.dsk')):
                obj = DWObject(fname)
                obj.wp = True
                dw.objects[dnum] = obj

        for n, obj in dw.objects.items():
            print(' - drive%d - %s   %s' %
                (n, obj.name, ['', '(read only)'][obj.wp]))

        print('ctrl-C to quit')
        dw.serverloop()

    except EnvironmentError as E:
        print(str(E))
